#!/usr/bin/env bash

CURRENT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}" )" && pwd)"

colorLeft="$(tmux show-options -gqv @batteryColorLeft)"
colorRight="$(tmux show-options -gqv @batteryColorRight)"
statusRight="$(tmux show-options -gqv status-right)"

[ -z "$colorLeft" ] && colorLeft="0"
[ -z "$colorRight" ] && colorRight="0"

searchStr="#{battery_indicator}"
execStr="#($CURRENT_DIR/status_bat $colorLeft $colorRight)"

statusRight=${statusRight//$searchStr/$execStr}

tmux set-option -g status-right "$statusRight"

